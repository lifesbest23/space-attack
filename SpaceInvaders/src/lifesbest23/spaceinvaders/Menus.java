package lifesbest23.spaceinvaders;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Menus implements MouseListener, KeyListener{
	private Game game;
	private MenuItem 	newGameEasy,
						newGameMedium,
						newGameHard,
						viewHighscore,
						quitGame,
						resumeGame,
						endGame,
						newGame,
						backFromHighscore,
						resetHighscore;
	
	public Menus(Game game){
		this.game = game;
		int i = 0;
		newGame = new MenuItem("New Game", 50, 50 + i++*25);
		newGameEasy = new MenuItem("EASY", 50, 50 + i++*25);
		newGameMedium = new MenuItem("MEDIUM", 50, 50 + i++*25);
		newGameHard = new MenuItem("HARD", 50, 50 + i++*25);
		resumeGame = new MenuItem("Resume Game", 50, 50 + i++*25);
		viewHighscore = new MenuItem("High Scores", 50, 50 + i++*25);
//		pauseGame = new MenuItem("", 50, 50 + i++*25);
		endGame = new MenuItem("End Game", 50, 50 + i++*25);
		quitGame = new MenuItem("Quit Game", 50, 50 + i++*25);
		resetHighscore = new MenuItem("Reset List", 50, 50 + i++*25);
		backFromHighscore = new MenuItem("Back", 50, 50 + i++*25);
	}
	
	public void drawMenu(Graphics2D g){
		if(game.mainScreen){
			g.setColor(Color.WHITE);
			g.drawString("SPACE INVADERS", 100, 35);
			newGame.draw(g);
			viewHighscore.draw(g);
			quitGame.draw(g);
		}
		else if(game.newGame){
			g.setColor(Color.WHITE);
			g.drawString("NEW GAME", 120, 35);
			newGameEasy.draw(g);
			newGameMedium.draw(g);
			newGameHard.draw(g);
		}
		else if(game.pause){
			g.setColor(Color.WHITE);
			g.drawString("PAUSE", 120, 35);
			resumeGame.draw(g);
			endGame.draw(g);
			viewHighscore.draw(g);
			quitGame.draw(g);
		}
		else if(game.gameOver){
			g.setColor(Color.BLACK);
			g.fillRect(50, 75, 200, 95);
			g.setColor(Color.WHITE);
			g.drawRect(50, 75, 200, 95);
			g.drawString("GAME OVER", 120, 35);
			g.drawString("Points :   " + game.score, 100, 100);
			g.drawString("Time   :   " + (int)(-game.startTime + game.end)/2000, 100, 120);
			g.drawLine(100, 135, 200, 135);
			g.drawString("Score  :   " + (game.score + (int)(-game.startTime + game.end)/2000), 100, 150);
			newGame.draw(g);
			viewHighscore.draw(g);
			quitGame.draw(g);
		}
		
		if(game.highScore){
			g.setColor(Color.BLACK);
			g.fillRect(50, 10, 200, 240);
			g.setColor(Color.WHITE);
			g.drawRect(50, 35, 200, 210);
			g.drawString("HIGH SCORE BOARD", 80, 30);
			g.drawString("Place", 70, 50);
			g.drawString("Score", 120, 50);
			for(int i = 0; i < 7 && i < game.highScores.length; i++){
				g.drawString(i + 1 + "        " , 70, 75 + i*25);
				g.drawString("" + game.highScores[game.highScores.length - i - 1], 120, 75 + i*25);
			}
			resetHighscore.draw(g);
			backFromHighscore.draw(g);
		}
	}
	
	private void perform(String str){
		if(str == newGameEasy.name){
			game.createNewGame = Game.EASY;
			game.gameOver = false;
			game.newGame = false;
		}
		if(str == newGameMedium.name){
			game.createNewGame = Game.MEDIUM;
			game.gameOver = false;
			game.newGame = false;
		}
		if(str == newGameHard.name){
			game.createNewGame = Game.HARD;
			game.gameOver = false;
			game.newGame = false;
		}
		if(str == viewHighscore.name){
			game.highScore = true;
		}
		if(str == quitGame.name){
			game.endGame();
		}
		if(str == resumeGame.name){
			game.pause = false;
		}
		if(str == endGame.name){
			game.running = false;
			game.pause = false;
			game.mainScreen = true;
		}
		if(str == newGame.name){
			game.mainScreen = false;
			game.running = false;
			game.pause = false;
			game.newGame = true;
		}
		if(str == backFromHighscore.name){
			game.highScore = false;
		}
		if(str == resetHighscore.name){
			game.highScores = new int[0];
			game.highScore = false;
		}
	}

	@Override public void mouseClicked(MouseEvent e) {}

	@Override public void mouseEntered(MouseEvent arg0) {}

	@Override public void mouseExited(MouseEvent arg0) {}

	@Override public void mousePressed(MouseEvent arg0) {}

	@Override public void mouseReleased(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		if(game.highScore){
			perform(resetHighscore.isOnItem(x, y));
			perform(backFromHighscore.isOnItem(x, y));
		}
		else if(game.mainScreen){
			perform(newGame.isOnItem(x, y));
			perform(viewHighscore.isOnItem(x, y));
			perform(quitGame.isOnItem(x, y));
		}
		else if(game.newGame){
			perform(newGameEasy.isOnItem(x, y));
			perform(newGameMedium.isOnItem(x, y));
			perform(newGameHard.isOnItem(x, y));
		}
		else if(game.pause){
			perform(resumeGame.isOnItem(x, y));
			perform(endGame.isOnItem(x, y));
			perform(viewHighscore.isOnItem(x, y));
			perform(quitGame.isOnItem(x, y));
		}
		else if(game.gameOver){
			perform(newGame.isOnItem(x, y));
			perform(viewHighscore.isOnItem(x, y));
			perform(quitGame.isOnItem(x, y));
		}
	}

	@Override public void keyPressed(KeyEvent e) {}

	@Override public void keyReleased(KeyEvent arg0) {}

	@Override public void keyTyped(KeyEvent e) {
		if(e.getKeyChar() == 'p'){
			if(game.pause)
				game.pause = false;
			else
				game.pause = true;
		}
	}
}
