package lifesbest23.spaceinvaders;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.ImageIcon;

public class Rock {
	
	private Game game;
	
	private int x;
	private int y;
	private int yv;
	
	private boolean visible = true;
	private Image image;
	
	private final int HEIGHT;
	private final int WIDTH;
	
	public Rock(Game game){
		initImage();
		
		this.game = game;

		WIDTH = image.getWidth(null);
		HEIGHT = image.getHeight(null);
		
		Random r = new Random();
		this.x = r.nextInt(game.GAME_WIDTH - WIDTH);
		this.y = 0;
		this.yv = game.speed / 2;
	}
	
	private void initImage(){
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/rock.png"));
		
		image = im.getImage();
	}
	
	public boolean isVisible(){
		return visible;
	}
	
	public void setVisible(boolean v){
		visible = v;
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}
	
	public void draw(Graphics2D g){
		g.drawImage(image, x, y, null);
	}
	
	public void move(){
		y += yv;
		
		if(y > game.GAME_HEIGHT - WIDTH){
			visible = false;
			game.life -= 2;
		}
	}
	
}
