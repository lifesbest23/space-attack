package lifesbest23.spaceinvaders;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Main extends JFrame{
	private static final long serialVersionUID = -3919162127967480238L;

	public Main(){
		super();
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.setTitle("SpaceInvaders");
		this.setIconImage(new ImageIcon(getClass().getResource("/resources/images/logo.png")).getImage());
		
		final Game g = new Game();
		this.add(g);
		
		this.addWindowListener(new WindowListener(){
			@Override public void windowActivated(WindowEvent arg0) {}
			@Override public void windowClosed(WindowEvent arg0) {
				g.endGame();
			}
			@Override public void windowClosing(WindowEvent arg0) {
				g.endGame();
			}
			@Override public void windowDeactivated(WindowEvent arg0) {}
			@Override public void windowDeiconified(WindowEvent arg0) {}
			@Override public void windowIconified(WindowEvent arg0) {}
			@Override public void windowOpened(WindowEvent arg0) {}
		});
		
		this.setSize(302, 432);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public static void main(String[] str){
		new Main();
	}

}
