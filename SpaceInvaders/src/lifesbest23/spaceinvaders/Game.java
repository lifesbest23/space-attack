package lifesbest23.spaceinvaders;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Game extends JPanel implements Runnable{
	private static final long serialVersionUID = -5828616935013935605L;
	
	public static final int EASY = 0;
	public static final int MEDIUM = 1;
	public static final int HARD = 2;
	
	public int GAME_WIDTH = 300;
	public int GAME_HEIGHT = 400;
	public final int MAX_LIFE = 200;
	
	public Menus menu;
	public int[] highScores = new int[0];
	
	public int createNewGame = -1;
	
	public Tank tank;
	
	public int score = 0;					//score
	public int life = MAX_LIFE;				//lifes
	public int speed = 2;					//moving speed
	public int alienShotDifference = 1000;	//min time difference between two alien shots
	public int tankShotDifference = 400;	//min time difference between two tank shots
	private int alienSpawnPossibility = 990;//Possibility for aliens to spawn out of 1000
	private int rockSpawnPossibility = 990;	//Possibility for Rocks to spawn out of 1000
	
	public List<Rock> rocks = new ArrayList<Rock>();
	public List<Alien> aliens = new ArrayList<Alien>();
	
	private Image bg;
	
	//Game states
	public boolean mainScreen = true;
	public boolean newGame = false;
	public boolean pause = false;
	public boolean running = false;
	public boolean gameOver = false;
	public boolean highScore = false;
	
	private Random r = new Random();

	public long startTime;
	public long start, end;
	private long max_delay = 35;
	
	public Game(){
		super();
		this.setFocusable(true);
		
		initImage();
		loadHighScores();
		
		tank = new Tank(this);
		this.addKeyListener(tank);
		
		menu = new Menus(this);
		this.addMouseListener(menu);
		this.addKeyListener(menu);
		
		new Thread(this).start();
		
	}
	
	private void initImage(){
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/bg.png"));
		bg = im.getImage();
	}
	
	public void loadHighScores(){
		File f = new File(".highScoreList");
		if(!f.exists())
			return;
		Scanner sw;
		try {
			sw = new Scanner(f);
			while(sw.hasNextInt()){
				highScores = Arrays.copyOf(highScores, highScores.length + 1);
				highScores[highScores.length - 1] = sw.nextInt();
			}
			sw.close();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	public void endGame(){
		File f = new File(".highScoreList");
		FileWriter fw;
		try {
			fw = new FileWriter(f);
			for(int i:highScores){
				fw.write(Integer.toString(i) + " ");
			}
			fw.close();
		} catch (IOException e) {e.printStackTrace();}
		System.exit(0);
	}
	
	public void gameOver(){
		highScores = Arrays.copyOf(highScores, highScores.length + 1);
		highScores[highScores.length - 1] = (int) (score + (System.currentTimeMillis()-startTime )/2000);
		Arrays.sort(highScores);
		running = false;
		gameOver = true;
	}
	
	public void paint(Graphics g2){
		super.paint(g2);
		
		Graphics2D g = (Graphics2D) g2;
		
//		g.setColor(Color.BLACK);
//		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.drawImage(bg, 0, 0, null);
		
		for(int i = 0; i < rocks.size(); i++)
			if(rocks.get(i).isVisible())
				rocks.get(i).draw(g);
		
		for(int i = 0; i < aliens.size(); i++)
			if(aliens.get(i).isVisible())
				aliens.get(i).draw(g);
		
		for(int j = 0; j < aliens.size(); j++){
			List<AlienBullet> alienbullets = aliens.get(j).getBullets();
			for(int i = 0; i < alienbullets.size(); i++)
				if(alienbullets.get(i).isVisible())
					alienbullets.get(i).draw(g);
		}
		
		List<Bullet> bullets = tank.getBullets();
		for(int i = 0; i < bullets.size(); i++)
			if(bullets.get(i).isVisible())
				bullets.get(i).draw(g);
		
		tank.draw(g);
		
		//life bar
		g.setColor(Color.BLACK);
		if(life > 0)
			g.fillRect(95 + life, 392, MAX_LIFE, 13);
		else
			g.fillRect(95, 392, MAX_LIFE, 13);
		//score
		g.setColor(new Color(0x909090));
		g.drawString(Integer.toString(score), 10, 403);
		//time
		int t = (int) (end - startTime) / 1000;
		g.drawString(Integer.toString(t), 50, 403);
		
		menu.drawMenu(g);
	}
	
	public void setPause(boolean p){
		pause = p;
	}

	@Override
	public void run() {
		while(true){
			if(createNewGame > -1 && createNewGame < 3)
				newGame(createNewGame);
			repaint();
			try {
				Thread.sleep(max_delay);
			} catch (InterruptedException e) {e.printStackTrace();}
		}
	}
	
	public void newGame(int dif){
		switch(dif){
		case EASY:
			alienSpawnPossibility = 990;
			rockSpawnPossibility = 988;
			alienShotDifference = 2000;
			tankShotDifference = 350;
			speed = 2;
			break;
		case MEDIUM:
			alienSpawnPossibility = 976;
			rockSpawnPossibility = 972;
			alienShotDifference = 1000;
			tankShotDifference = 270;
			speed = 3;
			break;
		case HARD:
			alienSpawnPossibility = 960;
			rockSpawnPossibility = 950;
			alienShotDifference = 500;
			tankShotDifference = 200;
			speed = 4;
			break;
		}
		life = MAX_LIFE;
		rocks = new ArrayList<Rock>();
		aliens = new ArrayList<Alien>();
		
		this.removeKeyListener(tank);
		tank = new Tank(this);
		this.addKeyListener(tank);
		
		createNewGame = -1;
		running = true;
		newGame();
	}
	
	public void newGame(){
		startTime = System.currentTimeMillis();
		while(running){
			if(!pause){
				start = System.currentTimeMillis();
				
				if(start - startTime % 30 == 0){
					rockSpawnPossibility -= 2;
					alienSpawnPossibility -= 2;
					alienShotDifference -= 75;
				}
				
				if(r.nextInt(1000) > rockSpawnPossibility + rocks.size())
					rocks.add(new Rock(this));
				if(r.nextInt(1000) > alienSpawnPossibility + aliens.size())
					aliens.add(new Alien(this));
				
				tank.move();
				
				List<Bullet> bullets = tank.getBullets();
				for(int i = bullets.size() - 1; i >= 0 ; i--){
					if(bullets.get(i).isVisible())
						bullets.get(i).move();
					else
						bullets.remove(i);
				}
				
				for(int i = rocks.size() - 1; i >= 0 ; i--){
					if(rocks.get(i).isVisible())
						rocks.get(i).move();
					else
						rocks.remove(i);
				}
				
				for(int i = aliens.size() - 1; i >= 0 ; i--){
					if(aliens.get(i).isVisible())
						aliens.get(i).move();
					else if(aliens.get(i).getBullets().size() == 0){
						aliens.remove(i);
						continue;
					}
					
					List<AlienBullet> alienbullets = aliens.get(i).getBullets();
					for(int j = 0; j < alienbullets.size(); j++){
						if(alienbullets.get(j).isVisible())
							alienbullets.get(j).move();
						else
							alienbullets.remove(j);
					}
				}
				collision();
			}
			repaint();
			try {
				end = System.currentTimeMillis();
				int d = (int) (max_delay - end + start);
				if(d < 2)
					Thread.sleep(2);
				else
					Thread.sleep(d);
			} catch (InterruptedException e) {e.printStackTrace();}
			
		}
	}
	
	private void collision(){
		List<Bullet> bullets = tank.getBullets();
		for(int i = bullets.size() - 1; i >= 0 ; i--){	//shot?
			if(bullets.get(i).isVisible()){
				for(int j = rocks.size() - 1; j >= 0 ; j--){
					if(rocks.get(j).isVisible())
						if(rocks.get(j).getBounds().intersects(bullets.get(i).getBounds())){//Rock?
							rocks.get(j).setVisible(false);
							bullets.get(i).setVisible(false);
							score += 1;
						}
				}
				for(int k = aliens.size() - 1; k >= 0 ; k--){
					if(aliens.get(k).isVisible())
						if(aliens.get(k).getBounds().intersects(bullets.get(i).getBounds())){//Alien
							aliens.get(k).setVisible(false);
							bullets.get(i).setVisible(false);
							score += 2;
						}
					List<AlienBullet> alienbullets = aliens.get(k).getBullets();
					for(int j = 0; j < alienbullets.size(); j++){
						if(alienbullets.get(j).isVisible())
							if(alienbullets.get(j).getBounds().intersects(bullets.get(i).getBounds())){//AlienBullet?
								alienbullets.get(j).setVisible(false);
								bullets.get(i).setVisible(false);
								score += 3;
							}
					}
				}
			}
		}
		//shot by Rock?
		for(int i = rocks.size() - 1; i >= 0 ; i--){
			if(rocks.get(i).isVisible())
				if(rocks.get(i).getBounds().intersects(tank.getBounds())){
					rocks.get(i).setVisible(false);
					life -= 10;
					if(life < 1){
						gameOver();
					}
				}
		}
		//shot by Alien?
		for(int k = aliens.size() - 1; k >= 0 ; k--){
			if(aliens.get(k).isVisible())
				if(aliens.get(k).getBounds().intersects(tank.getBounds())){
					aliens.get(k).setVisible(false);
					life -= 10;
					if(life < 1){
						gameOver();
					}
				}
			//shot by AlienBullet?
			List<AlienBullet> alienbullets = aliens.get(k).getBullets();
			for(int j = 0; j < alienbullets.size(); j++){
				if(alienbullets.get(j).isVisible())
					if(alienbullets.get(j).getBounds().intersects(tank.getBounds())){
						alienbullets.get(j).setVisible(false);
						life -= 10;
						if(life < 1){
							gameOver();
						}
					}
			}
		}
	}
}
