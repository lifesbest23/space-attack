package lifesbest23.spaceinvaders;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

public class Tank implements KeyListener{
	
	private Game game;
	
	private int x;
	private int xv = 0;
	private int y;
	
	private int dx_r, dx_l;
	
	private long lastShot = 0;
	private boolean shooting = false;
	
	private Image image;
	
	private List<Bullet> bullets = new ArrayList<Bullet>();
	
	private final int HEIGHT;
	private final int WIDTH;
	
	public Tank(Game game){
		initImage();
		WIDTH = image.getWidth(null);
		HEIGHT = image.getHeight(null);
		
		this.game = game;
		
		this.x = game.GAME_WIDTH / 2;
		this.y = game.GAME_HEIGHT - HEIGHT - 10;
	}
	
	private void initImage(){
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/tank.png"));
		image = im.getImage();
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}
	
	public List<Bullet> getBullets(){
		return bullets;
	}
	
	public void draw(Graphics2D g){
		g.drawImage(image, x, y, null);
	}
	
	public void move(){
		if(shooting)
			shoot();
		x += xv;
		
		if(x > game.GAME_WIDTH - WIDTH)
			x = 0;
		else if(x < 0)
			x = game.GAME_WIDTH - WIDTH;
	}
	
	private void shoot(){
		if(System.currentTimeMillis() - lastShot > game.tankShotDifference){
			bullets.add(new Bullet(x + 10, y - 10));
			lastShot = System.currentTimeMillis();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int d = game.speed + game.speed / 2;
		switch(e.getKeyCode()){
		case KeyEvent.VK_LEFT:
			if(dx_l == 0)
				xv -= d;
			else if(d == 0)
				xv += dx_l;
			dx_l = d;
			break;
		case KeyEvent.VK_RIGHT:
			if(dx_r == 0)
				xv += d;
			else if(d == 0)
				xv -= dx_r;
			dx_r = d;
			break;
		case KeyEvent.VK_SPACE:
			shooting = true;
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int d = 0;
		switch(e.getKeyCode()){
		case KeyEvent.VK_LEFT:
			if(dx_l == 0)
				xv -= d;
			else if(d == 0)
				xv += dx_l;
			dx_l = d;
			break;
		case KeyEvent.VK_RIGHT:
			if(dx_r == 0)
				xv += d;
			else if(d == 0)
				xv -= dx_r;
			dx_r = d;
			break;
		case KeyEvent.VK_SPACE:
			shooting = false;
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {}
}
