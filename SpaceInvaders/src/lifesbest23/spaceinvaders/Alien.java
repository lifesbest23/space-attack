package lifesbest23.spaceinvaders;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;

public class Alien {
	private static int idcount = 0;
	private int id = idcount++;
	
	private Game game;
	
	private int x;
	private int yv;
	private int y;
	
	private long lastShot = 0;
	
	private Image image;

	private boolean visible = true;
	
	private List<AlienBullet> bullets = new ArrayList<AlienBullet>();
	
	private Random r = new Random();
	
	private final int HEIGHT;
	private final int WIDTH;
	
	public Alien(Game game){
		initImage();
		WIDTH = image.getWidth(null);
		HEIGHT = image.getHeight(null);
		
		this.game = game;
		
		Random r = new Random();
		this.x = r.nextInt(game.GAME_WIDTH - WIDTH);
		this.y = 0;
		this.yv = game.speed;
	}
	
	private void initImage(){
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/alien.png"));
		image = im.getImage();
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}
	
	public List<AlienBullet> getBullets(){
		return bullets;
	}
	
	public void draw(Graphics2D g){
		g.drawImage(image, x, y, null);
	}
	
	public boolean isVisible(){
		return visible;
	}
	
	public void setVisible(boolean v){
		visible = v;
	}
	
	public void move(){
		if(r.nextInt(1000) > 992)
			shoot();
		
		y += yv;
		
		if(y > game.GAME_HEIGHT - WIDTH){
			visible = false;
			game.life -= 2;
		}
	}
	
	private void shoot(){
		if(System.currentTimeMillis() - lastShot > game.alienShotDifference){
			bullets.add(new AlienBullet(game, x + 10, y + 10));
			lastShot = System.currentTimeMillis();
		}
	}
	
	public String toString(){
		return String.format("Alien %d \t %d/%d %b\t %s", id, x, y, visible, bullets.toString());
	}
}
