package lifesbest23.spaceinvaders;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class MenuItem {
	public final String name;
	
	public final int HEIGHT = 20;
	public final int WIDTH = 200;
	private int x;
	private int y;
	
	public MenuItem(String str, int x, int y){
		name = str;
		
		this.x = x;
		this.y = y;
	}
	
	@SuppressWarnings("deprecation")
	public String isOnItem(int x, int y){
		if(getBounds().inside(x, y))
			return name;
		return null;
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}
	
	public void draw(Graphics2D g){
		g.setColor(Color.BLACK);
		g.fillRect(x, y, WIDTH, HEIGHT);
		g.setColor(Color.WHITE);		
		g.drawRect(x, y, WIDTH, HEIGHT);
		g.drawString(name, x + 20, y + 15);
	}
	
}
