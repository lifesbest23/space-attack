package lifesbest23.spaceinvaders;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class AlienBullet {
	private static int idcount = 0;
	private int id = idcount++;
	
	private Game game; 
	
	private int x;
	private int y;
	private int yv = 4;
	
	private final int HEIGHT = 8;
	private final int WIDTH = 4;
	
	private boolean visible = true;
	
	private Image image;
	
	public AlienBullet(Game game, int x, int y){
		initImage();
		
		this.game = game;
		
		this.x = x;
		this.y = y;
	}
	
	private void initImage(){
		ImageIcon im = new ImageIcon(getClass().getResource("/resources/images/alien_bullet.png"));
		
		image = im.getImage();
	}
	
	public boolean isVisible(){
		return visible;
	}
	
	public void setVisible(boolean v){
		visible = v;
	}
	
	public void draw(Graphics2D g){
		g.drawImage(image, x, y, null);
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}
	
	public void move(){
		if(y > game.GAME_HEIGHT - WIDTH){
			visible = false;
			game.life -= 2;
		}
		if(visible){
			y += yv;
		}
	}
	
	public String toString(){
		return String.format("AlienBullet %d \t %d/%d %b", id, x, y, visible);
	}
}
